import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { ProductlistService } from '../../service/productlist.service';
import { FormGroup, FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.scss']
})
export class ProductAddComponent implements OnInit {

  productForm:FormGroup;
  updateFrom:FormGroup;
  getId:any;
 
 


  constructor(
    public formBuider:FormBuilder,
    private route: Router,
    private activedRoute:ActivatedRoute,
    private ngZone:NgZone,
    private service:ProductlistService
  
  ) {
    this.productForm = this.formBuider.group({
      id: [''],
      name: [''],
      stock: [],
      price: [],
      time_create: [''],
      time_update: [''],

    }),
    this.getId = this.activedRoute.snapshot.paramMap.get('id')

    this.service.getProduct(this.getId).subscribe(res => {
      this.updateFrom.setValue({
        name:res['name'],
        stock:res['stock'],
        price:res['price'],

      })
    })
    this.updateFrom = this.formBuider.group({
      name:[''],
      stock:[''],
      price:[''],

    })
  }

  ngOnInit  (): void {


   
  }



  onSubmit():void{
 
    this.service.addProduct(this.productForm.value)
    .subscribe(() => {
      console.log("Data added");
      this.ngZone.run(()=> this.route.navigateByUrl('/'))
    })
  }



}
