import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { ProductlistService } from '../../service/productlist.service';
import { FormGroup, FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss']
})
export class ProductEditComponent implements OnInit {
  updateFrom:FormGroup;
  getId:any;

  constructor(
    public formBuider:FormBuilder,
    private route: Router,
    private activedRoute:ActivatedRoute,
    private ngZone:NgZone,
    private service:ProductlistService
  ) {
    this.getId = this.activedRoute.snapshot.paramMap.get('id')

    this.service.getProduct(this.getId).subscribe(res => {
      this.updateFrom.setValue({
        name:res['name'],
        stock:res['stock'],
        price:res['price'],

      })
    })
    this.updateFrom = this.formBuider.group({
      name:[''],
      stock:[''],
      price:[''],

    })
   }

  ngOnInit(): void {
  }

  onUpdate(): void{
  
    this.service.updateProduct(this.getId, this.updateFrom.value)
    .subscribe(() => {
      console.log("Data updated");
      this.ngZone.run(()=> this.route.navigateByUrl('/'))
    })
  }

}
