import { Component, OnInit } from '@angular/core';
import { ProductList } from '../../model/productlist';
import { ProductlistService } from '../../service/productlist.service';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  ProductList : ProductList[] = [];
  constructor(
    private service:ProductlistService
  ) { }

  ngOnInit  (): void{

    this.service.getProducts().subscribe(res =>{
      console.log(res)
      this.ProductList = res;
      
   
    })

    console.log('dsds',this.ProductList)
 
 

  }



  key:string = 'price';
  reverse:boolean =false;

  sort(key:string){
    this.key= key;
    this.reverse = !this.reverse

  }
 


  delete(id:string, i:number){
    console.log(id)
    if(window.confirm('Delete ?')){
      this.service.deleteProduct(id).subscribe((res) => {
        this.ProductList.splice(i,1);
      })

    }
  }


}
