import {  Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { ProductList } from '../model/productlist';
import { catchError, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ProductlistService {

  

  REST_API: string = 'http://localhost:8000/api';

  HttpHeaders = new HttpHeaders().set('Content-Type','application/json');


  
  constructor(private http:HttpClient) { }

  addProduct(data: ProductList): Observable<any>{
    let API_URL = `${this.REST_API}/add-product`;
    return this.http.post(API_URL,data)
    
  }

  getProducts(){
    return this.http.get<ProductList[]>(`${this.REST_API}`)
  }


  getProduct(id:string): Observable<ProductList>{
    let API_URL = `${this.REST_API}/read-product/${id}`;
    return this.http.get(API_URL,{ headers:this.HttpHeaders})
    .pipe(map((res:any) => {
      return res || {}
    }),
 
    )
  }

  updateProduct(id:string, data:ProductList): Observable<any>{
    let API_URL = `${this.REST_API}/update-product/${id}`;
    return this.http.put(API_URL, data, { headers:this.HttpHeaders})


  }


  deleteProduct(id:string):Observable<any>{
    let API_URL = `${this.REST_API}/delete-product/${id}`;
    return this.http.delete(API_URL, { headers:this.HttpHeaders})


  }

  




}
